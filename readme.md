xpi-downloader
===
This is a simple bash script that allows you to download firefox extensions
from the command line. Dependencies are wget and python3.

Usage
---
`./xpi-downloader [--help | -h | <addon URLs>]`

Example:
`./xpi-downloader https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/`

Credits
---
This is public domain software

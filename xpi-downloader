#!/bin/bash
# feel free to use this (public domain script)

usage() {
	echo "* usage: ./xpi-downloader [--help | <URLs>]"
}

download() {
	[ "$(which wget)" = "wget not found" ] && echo "✘ wget not found. Please install it before using xpi-downloader" && exit 1
	[ "$(which python3)" = "python3 not found" ] && echo "✘ python3 not found. Please install it before using xpi-downloader" && exit 1
	
	for url in "$@"
	do
		echo "* downloading HTML file from $url..."
		wget -t 0 -q "$url" -O tmp.html
		echo "* parsing file..."
		grep -o '<script type="application/json" id="redux-store-state">.*</script><script id=' "tmp.html" | sed 's/\(<script type="application\/json" id="redux-store-state">\|<\/script><script id=\)//g' > "tmp.json"
		rm -f "tmp.html"
		echo "* parsing the json file of the extension..."
		target_url="$(<tmp.json)"
		target_url="${target_url##*\"public\",\"url\":\"}"
		target_url="${target_url%%.xpi*}"
		target_url="$target_url.xpi"
		[ "$target_url" = "$(<tmp.json)" ] && rm -f "tmp.json" && echo "✘ couldn't find the link to the extension. Please check that the link is correct and try again." && exit 1
		rm -f "tmp.json"
		echo "* unescaping characters..."
		target_url=$(python3 -c "print('$target_url'.replace('\\u002F', '/'))")			
		echo "* target url found! → $target_url"
		echo "* downloading extension..."
		wget -t 0 -q "$target_url"
		echo "✔ extension successfully downloaded!"
	done

	echo "OK."
	exit 0
}

[ "$#" -eq 0 ] && usage && exit 1

case $1 in
	-h | --help)
		usage
		printf "* example: ./xpi-downloader https://addons.mozilla.org/ \n\
en-US/firefox/addon/ublock-origin/\n\n"
		echo "* options:"
		echo "-h, --help	Print this help message"
		echo "-a, --about	Print info about the program"
		echo ""
		echo "this is a public domain software"
		exit 0
		;;
	-a | --about)
		printf "* xpi-downloader lets you download firefox extensions \n\
without installing them and bypassing the user agent \n\
restrictions imposed by the Mozilla addon store. \n\
You need to install wget and python 3+ in order to use \n\
this script. Use ./xpi-downloader --help for brief usage info.\n"
		exit 0
		;;
	-* | --*)
		echo "✘ unknown command."
		usage
		exit 1
		;;
	*)
		download "$@"
		;;
esac

